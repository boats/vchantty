use std::fs::remove_file;
use std::io;
use std::os::unix::fs::symlink;
use std::path::PathBuf;

// This wrapper just makes the link deleted when destructors run
pub struct Symlink {
    link: String,
}

impl Symlink {
    pub fn new(real: PathBuf, link: String) -> io::Result<Symlink> {
        symlink(real, &link)?;
        Ok(Symlink { link })
    }
}

impl Drop for Symlink {
    fn drop(&mut self) {
        let _ = remove_file(&self.link);
    }
}
