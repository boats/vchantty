use std::fs::{read_link, File};
use std::io;
use std::os::unix::io::{FromRawFd, RawFd};
use std::os::unix::process::CommandExt;
use std::path::PathBuf;
use std::process::{Command, Stdio};
use std::ptr;

use libc;
use libc::{openpty, c_ushort, winsize, pid_t, close, signal, SIG_DFL};

fn open_pty(width: c_ushort, height: c_ushort) -> io::Result<(RawFd, RawFd)> {
    let mut term_fd = 0;
    let mut shell_fd = 0;
    let winsize = winsize {
        ws_row: height,
        ws_col: width,
        ws_xpixel: 0,
        ws_ypixel: 0,
    };
    let ret = unsafe {
        openpty(&mut term_fd as *mut _,
                &mut shell_fd as *mut _,
                ptr::null_mut(),
                ptr::null(),
                &winsize as *const _)
    };

    if ret >= 0 {
        Ok((term_fd, shell_fd))
    } else {
        Err(io::Error::last_os_error())
    }
}

fn run_cmd(cmd: &str, cmd_fd: RawFd, term_fd: RawFd) -> io::Result<pid_t> {
    let mut args = cmd.split_whitespace();
    let cmd = args.next().unwrap();
    let process = Command::new(cmd).args(args)
        .env("TERM", "xterm-256color")
        .stdin(unsafe { Stdio::from_raw_fd(cmd_fd) })
        .stdout(unsafe { Stdio::from_raw_fd(cmd_fd) })
        .stderr(unsafe { Stdio::from_raw_fd(cmd_fd) })
        .before_exec(move || unsafe {
            // create a new process group
            if libc::setsid() < 0 {
                return Err(io::Error::last_os_error());
            }

            if libc::ioctl(cmd_fd, libc::TIOCSCTTY, 0) < 0 {
                return Err(io::Error::last_os_error());
            }

            // close fds
            close(term_fd);
            close(cmd_fd);

            // set signals to default
            signal(libc::SIGCHLD, SIG_DFL);
            signal(libc::SIGHUP, SIG_DFL);
            signal(libc::SIGINT, SIG_DFL);
            signal(libc::SIGQUIT, SIG_DFL);
            signal(libc::SIGTERM, SIG_DFL);
            signal(libc::SIGALRM, SIG_DFL);

            Ok(())
        }).spawn()?;
    Ok(process.id() as _)
}

pub struct Pty {
    pub file: File,
    pub pid: pid_t,
    pub path: PathBuf,
}

impl Pty {
    pub fn prepare(setup: &::Setup) -> io::Result<Pty> {
        let (fd, child_fd) = open_pty(setup.width, setup.height)?;
        let path = read_link(format!("/proc/self/fd/{}", child_fd))?;
        let pid = run_cmd(&setup.cmd, child_fd, fd)?;
        let file = unsafe { File::from_raw_fd(fd) }; 

        Ok(Pty { file, pid, path })
    }
}
