use std::env;

use libc::c_ushort;
use uuid::Uuid;

pub struct Setup {
    pub cmd: String,
    pub width: c_ushort,
    pub height: c_ushort,
    pub uuid: Uuid,
}

impl Setup {
    pub fn prepare() -> Option<Setup> {
        let mut args = env::args().skip(1);
        let uuid = args.next()?.parse().ok()?;
        let height = args.next()?.parse().ok()?;
        let width = args.next()?.parse().ok()?;
        let mut cmd = args.next()?;
        for arg in args {
            cmd.push_str(" ");
            cmd.push_str(&arg);
        }
        Some(Setup { cmd, width, height, uuid })
    }
}

