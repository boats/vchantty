#![feature(catch_expr, mpsc_select)]

extern crate libc;
extern crate uuid;

mod setup;
mod symlink;
mod tty;

use std::io::{self, Read, Write};
use std::process::Command;
use std::sync::{mpsc, Arc};
use std::thread;

use setup::Setup;
use symlink::Symlink;
use tty::Pty;

pub fn main() {
    // Parse args and set up the child on a pty
    let setup = Setup::prepare().unwrap();
    let pty = Pty::prepare(&setup).unwrap();

    // Create a symlink from /var/vchanttyd/$uuid to the pty
    // 
    // This allows vchantty-resize to resize the file
    let symlink = Symlink::new(pty.path, format!("/home/user/.cache/vchanttyd/{}", setup.uuid));

    let pty = Arc::new(pty.file);
    let pty2 = pty.clone();
    let (tx1, rx1) = mpsc::channel();
    let (tx2, rx2) = mpsc::channel();

    thread::spawn(move || {
        let _: io::Result<()> = do catch {
            let stdin = io::stdin();
            let mut stdin = stdin.lock();
            let mut buf = [0; 4095];
            loop {
                let n = stdin.read(&mut buf)?;
                let mut init = 0;
                for (i, &b) in buf[..n].iter().enumerate() {
                    // Paste character
                    if b == b'\x16' {
                        (&*pty).write_all(&buf[init..i])?;
                        init = i + 1;

                        let paste = Command::new("xclip").arg("-o").output()?.stdout;
                        (&*pty).write_all(&paste)?
                    }
                }
                (&*pty).write_all(&buf[init..n])?;
            }
        };
        let _ = tx1.send(());
    });

    thread::spawn(move || {
        let _: io::Result<()> = do catch {
            let stdout = io::stdout();
            let mut stdout = stdout.lock();
            let mut buf = [0; 4095];
            // We can't use io::copy because we have to flush after every write
            //
            // TODO: instead, set stdout to not be line buffered
            loop {
                let n = (&*pty2).read(&mut buf)?;
                stdout.write_all(&buf[..n])?;
                stdout.flush()?;
            }
        };
        let _ = tx2.send(());
    });

    select! {
        _ = rx1.recv() => {},
        _ = rx2.recv() => {}
    };

    drop(symlink)
}
