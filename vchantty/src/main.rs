extern crate libc;
extern crate uuid;

use std::env;
use std::io;
use std::process::{Command, Stdio};

use libc::{ioctl, signal, tcgetattr, tcsetattr, termios, winsize, SIGWINCH, TIOCGWINSZ, TCSAFLUSH};
use uuid::Uuid;

static mut UUID: Option<Uuid> = None;
static mut DOMAIN: Option<String> = None;

extern "C" fn sigwinch(_a: libc::c_int) {
    if let (&Some(ref domain), &Some(ref uuid)) = unsafe { (&DOMAIN, &UUID) } {
        let (height, width) = get_wsize().unwrap();
        // execute `qvm-run {domain} "vchantty-resize {uuid} {height} {width}"`
        Command::new("qvm-run").arg(domain)
                .arg(format!("vchantty-resize {} {} {}", uuid, height, width))
                .output().unwrap();
    }
}

fn get_wsize() -> io::Result<(libc::c_ushort, libc::c_ushort)> {
    let mut winsize = winsize { ws_row: 0, ws_col: 0, ws_xpixel: 0, ws_ypixel: 0 };
    if 0 > unsafe { ioctl(0, TIOCGWINSZ, &mut winsize as *mut _) } {
        return Err(io::Error::last_os_error());
    }
    Ok((winsize.ws_row, winsize.ws_col))
}

fn setup_tty() -> io::Result<()> {
    let mut termios = termios {
        c_iflag: 0,
        c_oflag: 0,
        c_cflag: 0,
        c_lflag: 0,
        c_line: 0,
        c_cc: [0; 32],
        c_ispeed: 0,
        c_ospeed: 0,
    };

    unsafe {
        if 0 > tcgetattr(0, &mut termios as *mut _) {
            return Err(io::Error::last_os_error());
        }

        libc::cfmakeraw(&mut termios as *mut _);

        if 0 > tcsetattr(0, TCSAFLUSH, &termios as *const _) {
            return Err(io::Error::last_os_error());
        }
    }

    Ok(())
}

fn start_qube(domain: &str) -> io::Result<()> {
    let cmd = Command::new("qvm-start").arg("--skip-if-running").arg(&domain).output()?;
    if !cmd.status.success() {
        panic!("failed to start {}: {}", domain, String::from_utf8_lossy(&cmd.stdout))
    }
    Ok(())
}

fn main() {
    let mut args = env::args().skip(1);
    let domain = args.next().unwrap();
    let command = args.collect::<Vec<String>>().join(" ");
    let uuid = Uuid::new_v4();

    start_qube(&domain).unwrap();
    setup_tty().unwrap();

    let (height, width) = get_wsize().unwrap();
    
    // execute `qrexec-client -d {domain} "user:vchanttyd {uuid} {height} {width} {command}`
    let cmd = format!("user:vchanttyd {} {} {} {}", uuid, height, width, command);
    let mut child = Command::new("qrexec-client")
                            .arg("-d").arg(&domain).arg(cmd)
                            .stdin(Stdio::inherit())
                            .stdout(Stdio::inherit())
                            .spawn().unwrap();

    unsafe {
        DOMAIN = Some(domain);
        UUID = Some(uuid);
        signal(SIGWINCH, sigwinch as _);
    }

    child.wait().unwrap();
}
