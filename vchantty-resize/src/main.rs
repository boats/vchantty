extern crate libc;
extern crate uuid;

use std::env;
use std::fs::{read_link, File};
use std::os::unix::io::AsRawFd;

use uuid::Uuid;
use libc::{ioctl, winsize, TIOCSWINSZ};

fn main() {
    let mut args = env::args().skip(1);

    let uuid: Uuid = args.next().unwrap().parse().unwrap();
    let path = read_link(format!("/home/user/.cache/vchanttyd/{}", uuid)).unwrap();
    let pty = File::open(path).unwrap();

    let ws_row = args.next().unwrap().parse().unwrap();
    let ws_col = args.next().unwrap().parse().unwrap();
    let winsize = winsize { ws_row, ws_col, ws_xpixel: 0, ws_ypixel: 0 };

    unsafe { ioctl(pty.as_raw_fd(), TIOCSWINSZ, &winsize as *const _) };
}
